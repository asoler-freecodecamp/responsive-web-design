# Responsive Web Design Projects

These are the FreeCodeCamp Challenges for the Responsive Web Design Certificate

The Projects should be built in [Codepen.io](https://codepen.io/)

Each folder contains a project separated in a html, and a css file. No Javascript file needed.
